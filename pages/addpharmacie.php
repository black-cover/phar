<?php
require_once ('navbar.php');
?>

<!DOCTYPE html>
<html>
<style>

    body {font-family: Arial, Helvetica, sans-serif; }
    * {box-sizing: border-box}

    /* Full-width input fields */
    input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }
    input[type=date], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=date]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Set a style for all buttons */
    button {
        background-color: #25CCF7;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    button:hover {
        opacity:1;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
        padding: 14px 20px;
        background-color: #33d9b2;
    }

    /* Float cancel and signup buttons and add an equal width */
    .cancelbtn, .signupbtn {
        float: left;
        width: 50%;
    }

    /* Add padding to container elements */
    .container {
        padding: 16px;
    }

    /* Clear floats */
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    /* Change styles for cancel button and signup button on extra small screens */
    @media screen and (max-width: 300px) {
        .cancelbtn, .signupbtn {
            width: 100%;
        }
    }
</style>
<body>


<?php

$error_array = array();
if (!isset($_SESSION['id_user'])) {
    redirect("./acceuil.php");
}

if(isset($_POST['register'])){

    $name = escape_string($_POST['name']);
    $adress = escape_string($_POST['adress']);
    $local = escape_string($_POST['local']);
    $tele = escape_string($_POST['tele']);
    $date = escape_string($_POST['date']);
    $ville = escape_string($_POST['ville']);
    $datef = escape_string($_POST['datef']);
    $id = $_SESSION['id_user'];

    if($name == "" || $adress == "" || $local == "" || $ville == "" || $tele == "" || $date == ""){
        array_push($error_array, "Please Fill All inputs !!</span><br>");
    }else{
        $user_id =    $_SESSION['id_user'];
        $query1 = query("select * from phar where user_id = '$user_id'");
        if(mysqli_num_rows($query1) == 0){
            $query = query("insert into phar (nom,adresse,Localisation,user_id,telephon,ville,dateDeGarde,is_open,datef) values('$name','$adress','$local','$id','$tele','$ville','$date','1','$datef')");
            confirm($query);
            array_push($error_array, "pharmacie Created Successfully</span><br>");
        }else{
            array_push($error_array, "You Cant Add More Then One Pharmacie</span><br>");
        }

    }



}


?>


<form action="" method="post" style="border:1px solid #ccc">
    <div class="container">

        <h1>.</h1>
        <h1>ajouter pharmacie!</h1>
        <p>remplisser cette formulaire  .</p>
        <br>
        <?php
        if (in_array("Please Fill All inputs !!</span><br>", $error_array)) {
            echo "<h1><span style='color: #ed3228;'>Please Fill All inputs !! . </span></h1>";
        }
        if (in_array("pharmacie Created Successfully</span><br>", $error_array)) {
            echo "<h1><span style='color: greenyellow;'>Pharmacie Created Successfully . </span></h1>";
        }
        if (in_array("You Cant Add More Then One Pharmacie</span><br>", $error_array)) {
            echo "<h1><span style='color: #ed3228;'>You Cant Add More Then One Pharmacie !! . </span></h1>";
        }

        ?>
        <hr>
        <label for="firstName"><b>Nom de la pharmacie </b></label>
        <input type="text"  placeholder="entrer Nom de la pharmacie " name="name" required >
        <label for="tel"><b>Teléphone</b></label>
        <input type="text" placeholder="+212-666666666" name="tele" required>
        <select class="form-control" id="sel1" name="ville">
            <option value="FES">FES</option>
            <option value="casablanca">casablanca</option>
            <option value="tanger">tanger</option>
        </select>
        <label for="firstName"><b>date De début Garde </b></label>
        <input type="date"  placeholder="entrer date De Garde" name="date" required >
        <label for="firstName"><b>date De fin Garde </b></label>
        <input type="date"  placeholder="entrer date De Garde" name="datef" required >
        <label for="firstName"><b>Adresse </b></label>
        <input type="text"  placeholder="entrer Aadresse " name="adress" required >
              <div class="form-group">
              <label for="sel1"><b>Localisation </b></label>
              <select class="form-control" id="sel1" name="local">
                <option value="Agdal">Agdal</option>
                <option value="Fes Medina">Fes Medina</option>
                <option value="Jnan El Ward">Jnan El Ward</option>
                <option value="Les Mérinides">Les Mérinides</option>
                <option value="Saies">Saies</option>
                <option value="Zouagha">Zouagha</option>
                <option value="Ain Haroun">Ain Haroun</option>
                <option value="Ancienne Medina">Ancienne Medina</option>
                <option value="Ben Souda">Ben Souda</option>
                <option value="Dar Dbibegh">Dar Dbibegh</option>
                <option value="Hay Saada">Hay Saada</option>
                <option value="Oued Fès">Oued Fès</option>
                <option value="Mellah">Mellah</option>
                <option value="Mont fleuri">Mont fleuri</option>
                <option value="Route d'imozzar">Route d'imozzar</option>
                <option value="Tghat">Tghat</option>
                <option value="El Hadika">El Hadika</option>
                <option value="Al Atlas">Al Atlas</option>
                <option value="Hay agadir">Hay agadir</option>
                <option value="Hay mohamadi">Hay mohamadi</option>
                <option value="Zone industrielle sidi brahim">Zone industrielle sidi brahim</option>
              </select>
            </div>
        <div class="clearfix">
            <button type="submit" class="signupbtn" name="register"><a class="text-info"creer un compte>ajouter</a></button>
        </div>
    </div>
</form>



<div class="container">
    <h1><b>mon pharmacies</b></h1>
    <br>
    <div class="row">

    <?php
    if(isset($_GET['is'])){
        $is = $_GET['is'];
        $id = $_GET['id'];
        if($is == 0){
            $is = 1;
        }else{
            $is = 0;
        }
        $query = query("update phar set is_open = '$is' where id = '$id' ");
        confirm($query);
    }


    $idd = $_SESSION['id_user'];
        $query = query("select * from phar where user_id = $idd");
        confirm($query);
        while ($row = fetch_array($query)){
            ?>

                <div class="col-sm-6" style="border: 1px solid blanchedalmond">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><b>Nom de la pharmacie :</b> <?php echo $row['nom']?></h5>
                            <p class="card-text"> <b>Adresse :</b><?php echo $row['adresse']?>. <br>
                                <b>Localisation :</b><?php echo  $row['Localisation'] ?>. <br>
                                <b>Teléphone :</b><?php echo  $row['telephon'] ?>. <br>
                                <b>Ville :</b><?php echo  $row['ville'] ?>. <br>
                            </p>


                                <a href="addMedicaments.php?id=<?php echo $row['id']?>" class="btn btn-primary">ajouter des Médicaments</a>
                               <a href="addpharmacie.php?is=<?php echo $row['is_open']?>&id=<?php echo $row['id']?>" class="btn btn-info"><?php if($row['is_open'] == 0) echo 'Pharmacie ouverte '; else echo  'fermer'?></a>
                                <a href="delete.php?id=<?php echo $row['id']?>" onclick="return confirm('Are You Sure ?')" class="btn btn-danger">supprimer</a>


                        </div>
                    </div>
                </div>



    <?php
        }

    ?>
    </div>

</div>




</body>
</html>

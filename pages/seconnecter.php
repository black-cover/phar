<?php 
require_once ('navbar.php');
?>
<!DOCTYPE html>
<html>
<head>
    
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif; }
form {border: 1px  solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color:#25CCF7;
  color: white;
  padding: 8px 8px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: 15%;
  padding: 10px 10px;
  background-color: #33d9b2;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 20%;
  border-radius: 10%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}
     

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
  
}
</style>
 
    
    
    
</head>
    
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" >

<h2>Login Form</h2>


   
				
						
  <div class="imgcontainer">
    <img src="../images/img6.png" alt="Avatar" class="avatar">
  </div>

<?php
if (isset($_SESSION['remember'])) {
    redirect("./acceuil.php");
}
$error_array = array();

if(isset($_POST['login'])){
    $email = escape_string($_POST['email']);
    $password = escape_string($_POST['password']);
    $query = query("SELECT * FROM users WHERE email = '$email'");
    confirm($query);
    $row = fetch_array($query);
    if (mysqli_num_rows($query) == 0) {
        array_push($error_array, "Email or password was incorrect</span><br>");
    }
    if (password_verify($password, $row['password'])) {
        if (!empty($_POST['remember'])) {
            $_SESSION['remember'] = "true";
        }
        $_SESSION['is_login'] = "true";
        $_SESSION['id_user'] = $row['id'];
        redirect("./acceuil.php");
    } else {
        array_push($error_array, "Email or password was incorrect</span><br>");
    }
}



?>

<form action="" method="post">

    <div class="container">
        <?php
        if (in_array("Email or password was incorrect</span><br>", $error_array)) {
            echo "<h1><span style='color: #ed3228;'>Email or password was incorrect . </span></h1>";
        }
        ?>
        <label for="uname"><b>nom d'utilisateur</b></label>
        <input type="text" placeholder="entrer Email" name="email" required>

        <label for="psw"><b>mot de passe</b></label>
        <input type="password" placeholder="entrer le mot de passe" name="password" required>

        <button type="submit" href="../pages/profil.php" name="login">se connecter</button>
        <label>
            <input type="checkbox" checked="checked" name="remember"> sauvgarder le mot de passe
        </label>

    </div>

</form>


  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn"href="seconnecter.php"><a href="seconnecter.php"class="text-info"creer un compte>reinitialiser</a> </button>
       <button type="button" class="cancelbtn"><a href="register.php" class="text-info"creer un compte>creer un compte</a></button>
      
      
    <span class="psw"> <a href="../pages/initialisermotdepasse.php">mot de passe oublié?</a></span>
  </div>
    </div>
    
</form>

</body>
</html>
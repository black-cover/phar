
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>medicaments</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/style/medicaments.css">
        <?php
        require_once ('navbar.php');
        ?>
    </head>
    <body>

        <div class="container"></div>
        <div class="panel panel-info  margetop">
            <div class="panel-heading"><h3>Rechercher des Medicaments</h3>
            </div>
                <div class="panel-body">
                    <form action="" method="post">
                        <select class="form-control" id="sel1" name="s">

                            <option value="">Secteur</option>
                            <option value="Agdal">Agdal</option>
                            <option value="Fes Medina">Fes Medina</option>
                            <option value="Jnan El Ward">Jnan El Ward</option>
                            <option value="Les Mérinides">Les Mérinides</option>
                            <option value="Saies">Saies</option>
                            <option value="Zouagha">Zouagha</option>
                            <option value="Ain Haroun">Ain Haroun</option>
                            <option value="Ancienne Medina">Ancienne Medina</option>
                            <option value="Ben Souda">Ben Souda</option>
                            <option value="Dar Dbibegh">Dar Dbibegh</option>
                            <option value="Hay Saada">Hay Saada</option>
                            <option value="Oued Fès">Oued Fès</option>
                            <option value="Mellah">Mellah</option>
                            <option value="Mont fleuri">Mont fleuri</option>
                            <option value="Route d'imozzar">Route d'imozzar</option>
                            <option value="Tghat">Tghat</option>
                            <option value="El Hadika">El Hadika</option>
                            <option value="Al Atlas">Al Atlas</option>
                            <option value="Hay agadir">Hay agadir</option>
                            <option value="Hay mohamadi">Hay mohamadi</option>
                            <option value="Zone industrielle sidi brahim">Zone industrielle sidi brahim</option>

                        </select>
                        <br>
                        <input type="text"  class="form-control" name="r">
                        <button type="submit" class="btn btn-info" name="re">Rechercher </button>
                    </form>

                </div>
        </div>

         <div class="panel panel-success  margetop">
            <div class="panel-heading"><h3>Liste des Medicaments</h3>
            </div>
                <div class="panel-body">medicaments</div>
             <div class="container">

                 <br>
                 <div class="row">
             <?php

             if(isset($_POST['re'])){
                 $r = escape_string($_POST['r']);
                 $s = escape_string($_POST['s']);



                 if( $s  == ""){
                     $query = query("select name, prix,nom from medicaments inner join phar on (phar.id = medicaments.phar_id) where medicaments.name LIKE '%".$r."%' and  phar.is_open = 1");

                 }else{
                     $query = query("select name, prix,nom from medicaments inner join phar on (phar.id = medicaments.phar_id) where medicaments.name LIKE '%".$r."%' and phar.Localisation = '$s' and  phar.is_open = 1");

                 }
                     confirm($query);


                    while ($row = fetch_array($query)){
                    $m =     <<<deletemter
                        
            <div class="col-sm-6" style="border: 1px solid blanchedalmond">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><b>Nom  : {$row['name']}</b> </h5>
                        <p class="card-text"> <b>Prix :{$row['prix']}</b> <br>     
                        <p> <b>Nom de la pharmacie :</b>{$row['nom']} </p>          
                        </p>
                       

                    </div>
                </div>
            </div>
deletemter;
echo $m;
                    }
             }

             ?>
                 </div>
             </div>
        </div>
    </body>
</html>


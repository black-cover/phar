<?php
require_once ('navbar.php');
?>

<!DOCTYPE html>
<html>
<style>

    body {font-family: Arial, Helvetica, sans-serif; }
    * {box-sizing: border-box}

    /* Full-width input fields */
    input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }
    input[type=number], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }
    input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }
    input[type=number]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Set a style for all buttons */
    button {
        background-color: #25CCF7;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    button:hover {
        opacity:1;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
        padding: 14px 20px;
        background-color: #33d9b2;
    }

    /* Float cancel and signup buttons and add an equal width */
    .cancelbtn, .signupbtn {
        float: left;
        width: 50%;
    }

    /* Add padding to container elements */
    .container {
        padding: 16px;
    }

    /* Clear floats */
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    /* Change styles for cancel button and signup button on extra small screens */
    @media screen and (max-width: 300px) {
        .cancelbtn, .signupbtn {
            width: 100%;
        }
    }
</style>
<body>


<?php

if(!isset($_GET['id'])){
    redirect('acceuil.php');
}

$error_array = array();
if (!isset($_SESSION['id_user'])) {
    redirect("./acceuil.php");
}

if(isset($_POST['register'])){

    $name = escape_string($_POST['name']);
    $Prix = escape_string($_POST['Prix']);
    $quantity = escape_string($_POST['quantity']);
    $phar_id = $_GET['id'];

    if($name == "" || $Prix == "" || $quantity == ""){
        array_push($error_array, "Please Fill All inputs !!</span><br>");
    }else{
        $query = query("insert into medicaments (name,prix,quantity,phar_id) values('$name','$Prix','$quantity','$phar_id')");
        confirm($query);
        array_push($error_array, "pharmacie Created Successfully</span><br>");
    }



}


?>


<form action="addMedicaments.php?id=<?php echo  $_GET['id']?>" method="post" style="border:1px solid #ccc">
    <div class="container">

        <h1>.</h1>
        <h1>ajouter Médicament!</h1>
        <p>remplisser cette formulaire  .</p>
        <br>
        <?php
        if (in_array("Please Fill All inputs !!</span><br>", $error_array)) {
            echo "<h1><span style='color: #ed3228;'>Please Fill All inputs !! . </span></h1>";
        }
        if (in_array("pharmacie Created Successfully</span><br>", $error_array)) {
            echo "<h1><span style='color: greenyellow;'>Médicament Created Successfully . </span></h1>";
        }


        ?>
        <hr>
        <label for="firstName"><b>le nom de Médicament </b></label>
        <input type="text"  placeholder="entrer Nom de la pharmacie " name="name" required >
        <label for="firstName"><b>Prix</b></label>
        <input type="number"  placeholder="Prix " name="Prix" required >
        <label for="firstName"><b>quantity</b></label>
        <input type="number"  placeholder="quantity" name="quantity" required >
        <div class="clearfix">
            <button type="submit" class="signupbtn text-info" name="register">ajouter</button>
        </div>
    </div>
</form>



<div class="container">
    <h1><b>Médicaments</b></h1>
    <br>
    <div class="row">

        <?php



        $idd = $_GET['id'];
        $query = query("select * from medicaments where phar_id = $idd");
        confirm($query);
        while ($row = fetch_array($query)){
            ?>

            <div class="col-sm-6" style="border: 1px solid blanchedalmond">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><b>Nom de la Médicament :</b> <?php echo $row['name']?></h5>
                        <p class="card-text"> <b>Prix :</b><?php echo $row['prix']?>DH <br>
                            <b>Quantity :</b><?php echo  $row['quantity'] ?>
                        </p>
                        <?php
                            $phar = $_GET['id'];
                        ?>

                        <a href="modifierMedicaments.php?id=<?php echo $row['id']?> " class="btn btn-primary">modifier</a>

                        <a href="delete.php?phar=<?php echo $phar?>&idM=<?php echo $row['id']?>" onclick="return confirm('Are You Sure ?')" class="btn btn-danger">supprimer</a>


                    </div>
                </div>
            </div>



            <?php
        }

        ?>
    </div>

</div>




</body>
</html>

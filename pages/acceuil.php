<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
 body {
  font-family: Arial;
}

* {
  box-sizing: border-box;
}

form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 1px solid grey;
  float: left;
  width: 80%;
  background:   #fff;
}

form.example button {
  float: left;
  width: 20%;
  padding: 25px;
  background: #25CCF7;
  color: white;
  font-size: 17px;
  border: 1px solid grey;
  border-left: none;
  cursor: pointer;
}

form.example button:hover {
  background: #25CCF7;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 2s;
    -webkit-animation-duration: 2s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    }
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    }
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  </style>
<?php
require_once ('navbar.php');
?>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">


<div class="jumbotron text-center">
  <h2 style="color:black;">dans quel secteur vous-etes?</h2>
  <p style="color:black; ">****</p>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {
  font-family: Arial;
}

* {
  box-sizing: border-box;
}

form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 1px solid grey;
  float: left;
  width: 80%;
  background: #f1f1f1;
}

form.example button {
  float: left;
  width: 20%;
  padding: 10px;
  background: #2196F3;
  color: white;
  font-size: 17px;
  border: 1px solid grey;
  border-left: none;
  cursor: pointer;
}

form.example button:hover {
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}
</style>

 <form class="example" action="./search.php" method="post">
  <label for="sel1"><b>Secteur</b></label>

      <select class="form-control" id="sel1" name="r">

          <option value="">Secteur</option>
          <option value="Agdal">Agdal</option>
          <option value="Fes Medina">Fes Medina</option>
          <option value="Jnan El Ward">Jnan El Ward</option>
          <option value="Les Mérinides">Les Mérinides</option>
          <option value="Saies">Saies</option>
          <option value="Zouagha">Zouagha</option>
          <option value="Ain Haroun">Ain Haroun</option>
          <option value="Ancienne Medina">Ancienne Medina</option>
          <option value="Ben Souda">Ben Souda</option>
          <option value="Dar Dbibegh">Dar Dbibegh</option>
          <option value="Hay Saada">Hay Saada</option>
          <option value="Oued Fès">Oued Fès</option>
          <option value="Mellah">Mellah</option>
          <option value="Mont fleuri">Mont fleuri</option>
          <option value="Route d'imozzar">Route d'imozzar</option>
          <option value="Tghat">Tghat</option>
          <option value="El Hadika">El Hadika</option>
          <option value="Al Atlas">Al Atlas</option>
          <option value="Hay agadir">Hay agadir</option>
          <option value="Hay mohamadi">Hay mohamadi</option>
          <option value="Zone industrielle sidi brahim">Zone industrielle sidi brahim</option>

      </select>
   <button type="submit" name="search" class="btn btn-info">chercher</button>


</form>



</div>




<!-- Container (Portfolio Section) -->
<div id="portfolio" class="container-fluid text-center bg-gray">

  <div class="row text-center slideanim">

       <img src="../images/img4.jpg" class="img-thumbnail"  width="300" height="400">

      </div>
    </div>

 <br>

  <h2>avis des internautes</h2>
  <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <h4>"***********"<br><span>Meziane maryem</span></h4>
      </div>
      <div class="item">
        <h4>"je peux consulter la disponibilité des médicaments el ligne tout simplement... WOW!!"<br><span>aouinate niema</span></h4>
      </div>
      <div class="item">
        <h4>"« Je viens d’avoir un bébé il y a quelque mois. Votre site web m’a aidé plusieurs fois à trouver facilement la pharmacie qui est de garde dans mon quartier pendant la semaine »
"<br><span>benjelloun asmae</span></h4>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>






<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contactez-nous et vous recevz une reponse au bout de 24h.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Fès, morocco</p>
      <p><span class="glyphicon glyphicon-phone"></span> +212-611159274</p>
      <p><span class="glyphicon glyphicon-envelope"></span> hajar.bouaz20@gmail.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="nom" name="nom" placeholder="Nom" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Commentaire" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit" href="hajar.bouaz20@gmail.com">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Image of location/map -->

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>

</footer>

<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>

</body>
</html>


<?php

include('../config.php');


?>



<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" media="screen" type="text/css" href="../css/style/navbar.css">
</head>
<head>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="../index.php">Ma Pharmacie</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        
        <li><a href="pharmacie.php">PHARMACIE</a></li>
        <li><a href="medicaments.php">MEDICAMENTS</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="seconnecter.php">SE CONNECTER</a></li>
          <?php
          if (isset($_SESSION['remember'])) {
         ?>
          <li><a href="./addpharmacie.php">ajouter pharmacie</a></li>
          <li><a href="../logout.php">Log Out</a></li>
          <?php
          }
          ?>
      </ul>
    </div>
  </div>
</nav>
    
  
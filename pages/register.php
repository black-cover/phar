<?php 
require_once ('navbar.php');
?>

<!DOCTYPE html>
<html>
<style>

body {font-family: Arial, Helvetica, sans-serif; }
* {box-sizing: border-box}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
  background-color: #25CCF7;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

button:hover {
  opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
  padding: 14px 20px;
  background-color: #33d9b2;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
  padding: 16px;
}

/* Clear floats */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
  .cancelbtn, .signupbtn {
     width: 100%;
  }
}
</style>
<body>


<?php
if (isset($_SESSION['remember'])) {
    redirect("./acceuil.php");
}
$error_array = array();


if(isset($_POST['register'])){

    $email = escape_string($_POST['email']);
    $password = escape_string($_POST['password']);
    $repassword = escape_string($_POST['re-password']);
    $fname = escape_string($_POST['name']);
    $lname = escape_string($_POST['lname']);
    $phar = escape_string($_POST['phar']);
    $cin = escape_string($_POST['cin']);

    if($email == "" || $password == "" || $repassword == "" || $fname == "" || $lname == "" || $phar == "" || $cin == ""){
        array_push($error_array, "Please Fill All inputs !!</span><br>");
    }
    else{
        if($password != $repassword){
            array_push($error_array, "The Password Not Match !!</span><br>");

        }else{
            $query = query("SELECT * FROM users WHERE email = '$email'");
            confirm($query);
            $row = fetch_array($query);
            if (mysqli_num_rows($query) > 0) {
                array_push($error_array, "The Email Is Already Taken</span><br>");
            }else{
                $password = password_hash($password, PASSWORD_DEFAULT);
                $query = query("insert into users (nom,prenom,email,password,cin,pharmacie_id) values('$fname','$lname','$email','$password','$cin','$phar')");
                confirm($query);
                array_push($error_array, "Account Created Successfully Go Log In</span><br>");

            }
        }

    }




}


?>
    

<form action="" method="post" style="border:1px solid #ccc">
  <div class="container">
      
    <h1>Sign Up</h1>
      <h1>Regoignez-nous!</h1>
    <p>remplisser cette formulaire pour pouvoir acceder a votre espace personelle .</p>
      <br>
      <?php
      if (in_array("Please Fill All inputs !!</span><br>", $error_array)) {
          echo "<h1><span style='color: #ed3228;'>Please Fill All inputs !! . </span></h1>";
      }
      if (in_array("The Password Not Match !!</span><br>", $error_array)) {
          echo "<h1><span style='color: #ed3228;'>The Password Not Match !! . </span></h1>";
      }
      if (in_array("The Email Is Already Taken</span><br>", $error_array)) {
          echo "<h1><span style='color: #ed3228;'>The Email Is Already Taken !! . </span></h1>";
      }
      if (in_array("Account Created Successfully Go Log In</span><br>", $error_array)) {
          echo "<h1><span style='color: greenyellow;'>Account Created Successfully Go Log In . </span></h1>";
      }
      ?>
    <hr>
      <label for="firstName"><b>Nom</b></label>
    <input type="text"  placeholder="entrer votre nom" name="name" required >
      <label for="firstName"><b>Prénom</b></label>
    <input type="text"  placeholder="entrer votre Prénom" name="lname" required >
    
      <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Entrer Email" name="email" required>
      
<!--      <label for="tel"><b>Teléphone</b></label>-->
<!--    <input type="text" placeholder="+212-666666666" name="ttelephone" required>-->

<!--      <label for="patente"><b>Numero de Patente</b></label>-->
<!--    <input type="text" placeholder="n° de patente" name="tn" required>-->
      
      <label for="CIN"><b>Carte d'Identité Nationale</b></label>
    <input type="text" placeholder="entrer votre CIN" name="cin" required>
<!--     <label for="pharmacie"><b>Nom De La Pharmacie</b></label>-->
<!--    <input type="text" placeholder="entrer le nom" name="tpharmacie" required>-->

      <div class="form-group">
          <label for="sel1"><b>Nom De La Pharmacie</b></label>
          <select class="form-control" id="sel1" name="phar" required>
              <?php
                $query = query('select * from phar');
                confirm($query);
              while($row = fetch_array($query)) {
                  $product_cat =<<<DELIMETER
 <option value="{$row['id']}">{$row['nom']}</option>
DELIMETER;
                  echo $product_cat;
              }
              if (mysqli_num_rows($query) == 0) {
                  echo ' <option value="1">ph1</option>'
                  ?>
                  <?php
              }
              ?>

          </select>
      </div>

<!--      <div class="form-group">-->
<!--      <label for="sel1"><b>Secteur</b></label>-->
<!--      <select class="form-control" id="sel1" name="tsecteur">-->
<!--        <option>Secteur</option>-->
<!--        <option>Agdal</option>-->
<!--        <option>Fes Medina</option>-->
<!--        <option>Jnan El Ward</option>-->
<!--        <option>Les Mérinides</option>-->
<!--        <option>Saies</option>-->
<!--        <option>Zouagha</option>-->
<!--        <option>Ain Haroun</option>-->
<!--        <option>Ancienne Medina</option>-->
<!--        <option>Ben Souda</option>-->
<!--        <option>Dar Dbibegh</option>-->
<!--        <option>Hay Saada</option>-->
<!--        <option>Oued Fès</option>-->
<!--        <option>Mellah</option>-->
<!--        <option>Mont fleuri</option>-->
<!--        <option>Route d'imozzar</option>-->
<!--        <option>Tghat</option>-->
<!--        <option>El Hadika</option>-->
<!--        <option>Al Atlas</option>-->
<!--        <option>Hay agadir</option>-->
<!--        <option>Hay mohamadi</option>-->
<!--        <option>Zone industrielle sidi brahim</option>-->
<!--      </select>-->
<!--    </div>-->
<!--        <div class="form-group">-->
<!--      <label for="sel1">ville</label>-->
<!--      <select class="form-control" id="sel1" name="tville">-->
<!--        <option>ville</option>-->
<!--        <option>FES</option>-->
<!--      </select>-->
<!--    </div>-->

    <label for="password"><b>mot de passe</b></label>
    <input type="password" placeholder="Enter mot de passe" name="password" required>
    <label for="re-password"><b>confirmer le mot de passe </b></label>
    <input type="password" placeholder="Repeter le mot de passe" name="re-password" required>
    
<!--     <P> <label style="color:black"><b>Genre</b></label>-->
<!-- <input type="radio" id="femaleRadio" value="Female" name="tfemme">Femme-->
<!--  <input type="radio"  value="Male" name="thomme">Homme-->
<!--     -->
<!--    </P>-->
      

      
      <label>
      <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> sauvegarder les infos
    </label>
    
    

    <div class="clearfix">
      <button  class="cancelbtn"><a href="seconnecter.php" class="text-info"creer un compte>Go back To Login Page</a></button>
      <button type="submit" class="signupbtn" name="register"><a class="text-info"creer un compte>Register</a></button>
    </div>
  </div>
</form>

</body>
</html>

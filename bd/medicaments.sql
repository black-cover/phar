drop database if existe gestion_pharmacie;
create database if not exists gestion_pharmacie;

use gestion_pharmacie;

create table medicaments(
idMedicaments int(4) auto_increment primary key,
nomMedicaments varchar(50) not null ,
prixMedicaments float(10),
qtestock int(4)
);

create table pharmacie(
idPharmacie int(4) auto_increment primary key,
nomPharmacie varchar(50) not null ,
adressePharmacie varchar(10),
patentePharmacie int(20),
idsecteur varchar(20) 
) ;

create table secteur(
idSecteur int(4) auto_increment primary key,
nomSecteur varchar(50)
);

create table medpharma(
idPharmacie int(4) ,
idMedicaments int(4)
);

create table date_de_garde(
idDate int(4) auto_increment primary key,
DateDeGArde datetime(6),
idPharmacie int(4)
);

create table utilisateur(
iduser int(4) auto_increment primary key,
nomuser varchar(50) not null ,
numerouser int(15),
emailuser varchar(255),
cinuser VARCHAR(20),
sexuser varchar(20),
pswduser varchar(20)
) ;




alter table medpharma add constraint foreign key(idPharmacie,idMedicaments) references medicaments(idMedicaments),pharmacie(idPharmacie) ;

alter table date_de_garde add constraint foreign key(idPharmacie) references pharmacie(idPharmacie) ;

alter table pharmacie add constraint foreign key(idSecteur) references secteur(idSecteur) ;

INSERT INTO utilisateur VALUES 
	(1,'hajar','0611159274','hajar.bouaz20@gmail.com','289295','femme',md5('123az')),
	(2,'oumaima','0666666666','oumaimaelf@gmail.com','111111','femme',md5('123')),
    (3,'abdelali','0677777777','abdelalisaidi@gmail.com','22222','homme',md5('123'));


INSERT INTO medicaments VALUES 
	(1,'Doliprane',20 ,10),
    (2,'Exoderil',34.60 ,20),
    (3,'Predni',50.00 ,20),
    (4,'Polydexa',44.90 ,20),
    (5,'No-Dol',35 ,20),
    (6,'Tributine',120.67,20),
    (7,'Dolostop',70 ,20);
    
   
    select * from medicaments;
     select * from utilisateur;













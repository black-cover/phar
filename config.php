<?php


ob_start();

session_start();
//session_destroy();


//db config

defined("DH_HOST") ? null : define("DH_HOST", "localhost");

defined("DB_USER") ? null : define("DB_USER", "root");

defined("DB_PASSWORD") ? null : define("DB_PASSWORD", '');

defined("DB_NAME") ? null : define("DB_NAME", "gestion_pharmacie");
$timezone = date_default_timezone_set("Africa/Casablanca");
$connection = mysqli_connect(DH_HOST,DB_USER,DB_PASSWORD,DB_NAME);

if(!$connection){
    echo "sorry we cant connect to database";
}

require_once("functions.php");

